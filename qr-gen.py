import argparse
import base64
import json
import pyotp
import qrcode
import xml.etree.ElementTree as ET


# Requires following packages to be installed:
#   dnf install python3-pyotp python3-qrcode
#
# create a backup using the following command:
#   adb backup org.fedorahosted.freeotp
#
# extract the backup file using:
#   dd if=freeotp.adb bs=24 skip=1 | openssl zlib -d | tar xf -
#
# run this script using:
#   qr-gen.py apps/org.fedorahosted.freeotp/sp/tokens.xml


parser = argparse.ArgumentParser()
parser.add_argument('xmlfile')

args = parser.parse_args()

tree = ET.parse(args.xmlfile)
root = tree.getroot()

strings = root.findall('string')

for string in strings:
    token = json.loads(string.text)
    secret = []
    for byte in token['secret']:
        if byte < 0:
            secret.append(256 + byte)
        else:
            secret.append(byte)

    sec = base64.b32encode(bytes(secret))

    if token['type'] == 'HOTP':
        initial_count = token['counter'] + 1
    else:
        initial_count = None

    qrdata = pyotp.utils.build_uri(
            sec,
            token['label'],
            initial_count=initial_count,
            issuer_name=token.get('issuerExt', None),
            algorithm=token['algo'],
            digits=token['digits'],
            period=token['period'])

    qr = qrcode.QRCode()
    qr.add_data(qrdata)

    print(token['label'])
    qr.print_ascii(invert=True)
